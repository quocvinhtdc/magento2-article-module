<?php
/**
 * Author: Duong Quoc Vinh
 * Date: 3/10/2019
 */

namespace SM\Article\Controller\Index;
use \Magento\Framework\App\Action\Action;
use \Magento\Framework\App\Action\Context;
use Magento\Framework\View\Result\PageFactory;

use SM\Article\Model\ArticleFactory;

/**
 * Class AddArticle
 * @package SM\Article\Controller\Index
 */
class AddArticle extends Action
{
    /**
     * @var PageFactory
     */
    protected $_pageFactory;

    /**
     * @var ArticleFactory
     */
    protected $_articleFactory;

    /**
     * @var
     */
    protected $_collectionFactory;

    /**
     * AddArticle constructor.
     * @param Context $context
     * @param PageFactory $pageFactory
     * @param ArticleFactory $articleFactory
     */
    public function __construct(
        Context $context,
        PageFactory $pageFactory,
        ArticleFactory $articleFactory
    )
    {
        $this->_articleFactory = $articleFactory;
        $this->_pageFactory = $pageFactory;
        return parent::__construct($context);
    }

    /**
     * @return \Magento\Framework\App\ResponseInterface|\Magento\Framework\Controller\ResultInterface|\Magento\Framework\View\Result\Page
     */
    public function execute()
    {
        if (isset($_POST['add_article'])) {
            $data = $_POST;
            $model = $this->_articleFactory->create();
            $model->addData(
                [
                    'title' => $data['title'],
                    'content' => $data['content'],
                    'image' => $data['image']
                ]
            );
            $model->save();
        }
        return $this->_pageFactory->create();
    }
}
