<?php
/**
 * Author: Duong Quoc Vinh
 * Date: 3/10/2019
 */

namespace SM\Article\Controller\Index;

use Magento\Framework\App\Action\Action;
use Magento\Framework\App\Action\Context;
use Magento\Framework\View\Result\PageFactory;
use SM\Article\Helper\Data;

/**
 * Class AllArticle
 * @package SM\Article\Controller\Index
 */
class AllArticle extends Action
{
    /**
     * @var PageFactory
     */
    protected $_pageFactory;

    /**
     * get config enable and limit form admin page
     *
     * @var Data
     */
    protected $_helpData;

    /**
     * AllArticle constructor.
     * @param Data $helpData
     * @param Context $context
     * @param PageFactory $pageFactory
     */
    public function __construct(
        Data $helpData,
        Context $context,
        PageFactory $pageFactory)
    {
        $this->_helpData = $helpData;
        $this->_pageFactory = $pageFactory;
        return parent::__construct($context);
    }

    /**
     * @return \Magento\Framework\App\ResponseInterface|\Magento\Framework\Controller\ResultInterface|\Magento\Framework\View\Result\Page
     */
    public function execute()
    {
        $enableConfig = $this->_helpData->getGeneralConfig('enable');
        if(!$enableConfig) {
            $this->_redirect("/not-found");
        }
        return $this->_pageFactory->create();
    }
}
