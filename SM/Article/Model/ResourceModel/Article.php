<?php
/**
 * Author: Duong Quoc Vinh
 * Date: 3/10/2019
 */

namespace SM\Article\Model\ResourceModel;

/**
 * Class Article
 * @package SM\Article\Model\ResourceModel
 */
class Article extends \Magento\Framework\Model\ResourceModel\Db\AbstractDb
{
    /**
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init('sm_article', 'article_id');
    }
}