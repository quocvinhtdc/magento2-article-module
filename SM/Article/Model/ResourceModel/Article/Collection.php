<?php
/**
 * Author: Duong Quoc Vinh
 * Date: 3/10/2019
 */

namespace SM\Article\Model\ResourceModel\Article;

use \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection;

/**
 * Class Collection
 * @package SM\Article\Model\ResourceModel\Article
 */
class Collection extends AbstractCollection
{
    /**
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init(\SM\Article\Model\Article::class, \SM\Article\Model\ResourceModel\Article::class);
    }


}
