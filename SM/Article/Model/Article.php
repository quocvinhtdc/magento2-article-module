<?php
/**
 * Author: Duong Quoc Vinh
 * Date: 3/10/2019
 */
namespace SM\Article\Model;

/**
 * Class Article
 * @package SM\Article\Model
 */
class Article extends \Magento\Framework\Model\AbstractModel
{
    /**
     * Initialize banner model
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init(\SM\Article\Model\ResourceModel\Article::class);
    }
}