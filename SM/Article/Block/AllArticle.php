<?php
/**
 * Author: Duong Quoc Vinh
 * Date: 3/10/2019
 */

namespace SM\Article\Block;

use Magento\Framework\View\Element\Template;
use \Magento\Framework\View\Element\Template\Context;
use \SM\Article\Model\ResourceModel\Article\CollectionFactory;
use SM\Article\Helper\Data;

/**
 * Load all article with panigation
 * Block Article
 */
class AllArticle extends Template
{
    /**
     * all article
     *
     * @var CollectionFactory
     */
    public $_collectionFactory;

    /**
     * get config enable and limit form admin page
     *
     * @var Data
     */
    protected $_helpData;

    /**
     * AllArticle constructor.
     * @param Data $helpData
     * @param Context $context
     * @param CollectionFactory $collectionFactory
     * @param array $data
     */
    public function __construct(
        Data $helpData,
        Context $context,
        CollectionFactory $collectionFactory,
        array $data = []
    )
    {
        $this->_helpData = $helpData;
        $this->_collectionFactory = $collectionFactory;
        parent::__construct($context, $data);
    }

    /**
     * @return \SM\Article\Model\ResourceModel\Article\Collection
     */
    public function getArticlesCollection()
    {
        $page = ($this->getRequest()->getParam('p')) ? $this->getRequest()->getParam('p') : 1;
        $pageSize = $this->_helpData->getGeneralConfig('limit_per_page');
        $collection = $this->_collectionFactory->create();
        $collection->setOrder('article_id', 'ASC');
        $collection->setPageSize($pageSize);
        $collection->setCurPage($page);
        return $collection;
    }

    /**
     * @return Template
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function _prepareLayout()
    {
        $this->pageConfig->getTitle()->set(__('Articles'));
        if ($this->getArticlesCollection()) {
            $pager = $this->getLayout()->createBlock(
                'Magento\Theme\Block\Html\Pager',
                'sm.article.pager'
            )->setAvailableLimit(array(10 => 10, 15 => 15, 20 =>20))->setShowPerPage(false)->setCollection(
                $this->getArticlesCollection()
            );
            $this->setChild('pager', $pager);
            $this->getArticlesCollection()->load();
        }
        return parent::_prepareLayout();
    }

    /**
     * @return string
     */
    public function getPagerHtml()
    {
        return $this->getChildHtml('pager');
    }
}
