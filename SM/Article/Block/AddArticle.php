<?php
/**
 * Author: Duong Quoc Vinh
 * Date: 3/10/2019
 */

namespace SM\Article\Block;

use \Magento\Framework\View\Element\Template;
use \Magento\Framework\View\Element\Template\Context;
use \SM\Article\Model\ResourceModel\Article\CollectionFactory;

/**
 * Class AddArticle
 * @package SM\Article\Block
 */
class AddArticle extends Template
{
    /**
     * @var CollectionFactory
     */
    public $collectionFactory;

    /**
     * Display constructor.
     * @param Context $context
     * @param CollectionFactory $collectionFactory
     */
    public function __construct(Context $context, CollectionFactory $collectionFactory)
    {
        $this->collectionFactory = $collectionFactory;
        parent::__construct($context);
    }

    /**
     * Set title of add article
     *
     * @return Template
     */
    public function _prepareLayout()
    {
        $this->pageConfig->getTitle()->set(__('Add Article'));
        return parent::_prepareLayout();
    }
}