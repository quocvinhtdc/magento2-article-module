<?php
/**
 * Author: Duong Quoc Vinh
 * Date: 3/10/2019
 */

namespace SM\Article\Block;

use Magento\Framework\View\Element\Template;
use \Magento\Framework\View\Element\Template\Context;
use \SM\Article\Model\ResourceModel\Article\CollectionFactory;
use Magento\Framework\App\Request\Http;

/**
 * Class Detail
 * @package SM\Article\Block
 */
class Detail extends Template
{
    /**
     * @var CollectionFactory
     */
    public $_collectionFactory;

    /**
     * @var Http
     */
    public $_request;

    /**
     * Detail constructor.
     * @param Context $context
     * @param CollectionFactory $collectionFactory
     * @param Http $request
     */
    public function __construct(
        Context $context,
        CollectionFactory $collectionFactory,
        Http $request
    )
    {
        $this->_collectionFactory = $collectionFactory;
        $this->_request = $request;
        parent::__construct($context);
    }

    /**
     * get article by article_id
     *
     * @return array
     */
    public function getArticleDetail()
    {
        $idArticle = $this->_request->getParams();
        return $this->_collectionFactory->create()->addFieldToFilter(['article_id'], [$idArticle['id']])->getData();
    }

    /**
     * Set title of detail page
     *
     * @return Template
     */
    public function _prepareLayout()
    {
        $this->pageConfig->getTitle()->set(__('Article Detail'));
        return parent::_prepareLayout();
    }
}
