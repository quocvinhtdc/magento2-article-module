<?php
/**
 * Author: Duong Quoc Vinh
 * Date: 3/10/2019
 */

namespace SM\Article\Helper;

use Magento\Framework\App\Helper;
use Magento\Framework\App\Helper\AbstractHelper;
use Magento\Store\Model\ScopeInterface;

/**
 * Class Data
 * @package SM\Article\Helper
 */
class Data extends AbstractHelper {

    /**
     * set default section name (article/configuration/{{ $value }})
     */
    const XML_PATH_ARTICLE = "article/configuration/";

    /**
     * @param $field
     * @param null $storeId
     * @return mixed
     */
    public function getConfigValue($field, $storeId = null) {
        return $this->scopeConfig->getValue(
            $field,
            ScopeInterface::SCOPE_STORE,
            $storeId);
    }

    /**
     * @param $code
     * @param null $storeId
     * @return mixed
     */
    public function getGeneralConfig($code, $storeId = null) {
        return $this->getConfigValue(self::XML_PATH_ARTICLE. $code,$storeId);
    }

}