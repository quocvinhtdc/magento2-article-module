<?php

namespace SM\Article\Setup;

use Magento\Framework\Setup\InstallDataInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\ModuleDataSetupInterface;
use Magento\Framework\Stdlib\DateTime\DateTime;

class InstallData implements InstallDataInterface
{
    protected $date;

    public function __construct(DateTime $date) {
        $this->date = $date;
    }

    public function install(ModuleDataSetupInterface $setup, ModuleContextInterface $context)
    {
        $dataNewsRows = [
            [
                'title' => "Hà Nội cấm nhiều tuyến phố để thi công đường đua F1 ",
                'content' => "Cụ thể, theo Sở GTVT Hà Nội, khu vực xây dựng đường đua xe F1 nằm trong địa giới hành chính gồm các phường Mỹ Đình 1, Mỹ Đình 2, Mễ Trì, Phú Đô với tổng chiều dài 5,57km. Khu vực phải tổ chức đảm bảo an toàn giao thông thuộc phạm vi các đường: Lê Đức Thọ, Lê Quang Đạo
                 (rào chắn toàn bộ nút giao Châu Văn Liêm đến nút giao Tân Mỹ) có chiều dài 4km/2 chiều.",
                'image' => "hinh1.jpg"
            ],
            [
                'title' => "Hà Nội cấm nhiều tuyến phố để thi công đường đua F1 ",
                'content' => "Cụ thể, theo Sở GTVT Hà Nội, khu vực xây dựng đường đua xe F1 nằm trong địa giới hành chính gồm các phường Mỹ Đình 1, Mỹ Đình 2, Mễ Trì, Phú Đô với tổng chiều dài 5,57km. Khu vực phải tổ chức đảm bảo an toàn giao thông thuộc phạm vi các đường: Lê Đức Thọ, Lê Quang Đạo
                 (rào chắn toàn bộ nút giao Châu Văn Liêm đến nút giao Tân Mỹ) có chiều dài 4km/2 chiều.",
                'image' => "hinh1.jpg"
            ],  [
                'title' => "Hà Nội cấm nhiều tuyến phố để thi công đường đua F1 ",
                'content' => "Cụ thể, theo Sở GTVT Hà Nội, khu vực xây dựng đường đua xe F1 nằm trong địa giới hành chính gồm các phường Mỹ Đình 1, Mỹ Đình 2, Mễ Trì, Phú Đô với tổng chiều dài 5,57km. Khu vực phải tổ chức đảm bảo an toàn giao thông thuộc phạm vi các đường: Lê Đức Thọ, Lê Quang Đạo
                 (rào chắn toàn bộ nút giao Châu Văn Liêm đến nút giao Tân Mỹ) có chiều dài 4km/2 chiều.",
                'image' => "hinh1.jpg"
            ],  [
                'title' => "Hà Nội cấm nhiều tuyến phố để thi công đường đua F1 ",
                'content' => "Cụ thể, theo Sở GTVT Hà Nội, khu vực xây dựng đường đua xe F1 nằm trong địa giới hành chính gồm các phường Mỹ Đình 1, Mỹ Đình 2, Mễ Trì, Phú Đô với tổng chiều dài 5,57km. Khu vực phải tổ chức đảm bảo an toàn giao thông thuộc phạm vi các đường: Lê Đức Thọ, Lê Quang Đạo
                 (rào chắn toàn bộ nút giao Châu Văn Liêm đến nút giao Tân Mỹ) có chiều dài 4km/2 chiều.",
                'image' => "hinh1.jpg"
            ],  [
                'title' => "Hà Nội cấm nhiều tuyến phố để thi công đường đua F1 ",
                'content' => "Cụ thể, theo Sở GTVT Hà Nội, khu vực xây dựng đường đua xe F1 nằm trong địa giới hành chính gồm các phường Mỹ Đình 1, Mỹ Đình 2, Mễ Trì, Phú Đô với tổng chiều dài 5,57km. Khu vực phải tổ chức đảm bảo an toàn giao thông thuộc phạm vi các đường: Lê Đức Thọ, Lê Quang Đạo
                 (rào chắn toàn bộ nút giao Châu Văn Liêm đến nút giao Tân Mỹ) có chiều dài 4km/2 chiều.",
                'image' => "hinh1.jpg"
            ],  [
                'title' => "Hà Nội cấm nhiều tuyến phố để thi công đường đua F1 ",
                'content' => "Cụ thể, theo Sở GTVT Hà Nội, khu vực xây dựng đường đua xe F1 nằm trong địa giới hành chính gồm các phường Mỹ Đình 1, Mỹ Đình 2, Mễ Trì, Phú Đô với tổng chiều dài 5,57km. Khu vực phải tổ chức đảm bảo an toàn giao thông thuộc phạm vi các đường: Lê Đức Thọ, Lê Quang Đạo
                 (rào chắn toàn bộ nút giao Châu Văn Liêm đến nút giao Tân Mỹ) có chiều dài 4km/2 chiều.",
                'image' => "hinh1.jpg"
            ],  [
                'title' => "Hà Nội cấm nhiều tuyến phố để thi công đường đua F1 ",
                'content' => "Cụ thể, theo Sở GTVT Hà Nội, khu vực xây dựng đường đua xe F1 nằm trong địa giới hành chính gồm các phường Mỹ Đình 1, Mỹ Đình 2, Mễ Trì, Phú Đô với tổng chiều dài 5,57km. Khu vực phải tổ chức đảm bảo an toàn giao thông thuộc phạm vi các đường: Lê Đức Thọ, Lê Quang Đạo
                 (rào chắn toàn bộ nút giao Châu Văn Liêm đến nút giao Tân Mỹ) có chiều dài 4km/2 chiều.",
                'image' => "hinh1.jpg"
            ],  [
                'title' => "Hà Nội cấm nhiều tuyến phố để thi công đường đua F1 ",
                'content' => "Cụ thể, theo Sở GTVT Hà Nội, khu vực xây dựng đường đua xe F1 nằm trong địa giới hành chính gồm các phường Mỹ Đình 1, Mỹ Đình 2, Mễ Trì, Phú Đô với tổng chiều dài 5,57km. Khu vực phải tổ chức đảm bảo an toàn giao thông thuộc phạm vi các đường: Lê Đức Thọ, Lê Quang Đạo
                 (rào chắn toàn bộ nút giao Châu Văn Liêm đến nút giao Tân Mỹ) có chiều dài 4km/2 chiều.",
                'image' => "hinh1.jpg"
            ],  [
                'title' => "Hà Nội cấm nhiều tuyến phố để thi công đường đua F1 ",
                'content' => "Cụ thể, theo Sở GTVT Hà Nội, khu vực xây dựng đường đua xe F1 nằm trong địa giới hành chính gồm các phường Mỹ Đình 1, Mỹ Đình 2, Mễ Trì, Phú Đô với tổng chiều dài 5,57km. Khu vực phải tổ chức đảm bảo an toàn giao thông thuộc phạm vi các đường: Lê Đức Thọ, Lê Quang Đạo
                 (rào chắn toàn bộ nút giao Châu Văn Liêm đến nút giao Tân Mỹ) có chiều dài 4km/2 chiều.",
                'image' => "hinh1.jpg"
            ],  [
                'title' => "Hà Nội cấm nhiều tuyến phố để thi công đường đua F1 ",
                'content' => "Cụ thể, theo Sở GTVT Hà Nội, khu vực xây dựng đường đua xe F1 nằm trong địa giới hành chính gồm các phường Mỹ Đình 1, Mỹ Đình 2, Mễ Trì, Phú Đô với tổng chiều dài 5,57km. Khu vực phải tổ chức đảm bảo an toàn giao thông thuộc phạm vi các đường: Lê Đức Thọ, Lê Quang Đạo
                 (rào chắn toàn bộ nút giao Châu Văn Liêm đến nút giao Tân Mỹ) có chiều dài 4km/2 chiều.",
                'image' => "hinh1.jpg"
            ],  [
                'title' => "Hà Nội cấm nhiều tuyến phố để thi công đường đua F1 ",
                'content' => "Cụ thể, theo Sở GTVT Hà Nội, khu vực xây dựng đường đua xe F1 nằm trong địa giới hành chính gồm các phường Mỹ Đình 1, Mỹ Đình 2, Mễ Trì, Phú Đô với tổng chiều dài 5,57km. Khu vực phải tổ chức đảm bảo an toàn giao thông thuộc phạm vi các đường: Lê Đức Thọ, Lê Quang Đạo
                 (rào chắn toàn bộ nút giao Châu Văn Liêm đến nút giao Tân Mỹ) có chiều dài 4km/2 chiều.",
                'image' => "hinh1.jpg"
            ],  [
                'title' => "Hà Nội cấm nhiều tuyến phố để thi công đường đua F1 ",
                'content' => "Cụ thể, theo Sở GTVT Hà Nội, khu vực xây dựng đường đua xe F1 nằm trong địa giới hành chính gồm các phường Mỹ Đình 1, Mỹ Đình 2, Mễ Trì, Phú Đô với tổng chiều dài 5,57km. Khu vực phải tổ chức đảm bảo an toàn giao thông thuộc phạm vi các đường: Lê Đức Thọ, Lê Quang Đạo
                 (rào chắn toàn bộ nút giao Châu Văn Liêm đến nút giao Tân Mỹ) có chiều dài 4km/2 chiều.",
                'image' => "hinh1.jpg"
            ],  [
                'title' => "Hà Nội cấm nhiều tuyến phố để thi công đường đua F1 ",
                'content' => "Cụ thể, theo Sở GTVT Hà Nội, khu vực xây dựng đường đua xe F1 nằm trong địa giới hành chính gồm các phường Mỹ Đình 1, Mỹ Đình 2, Mễ Trì, Phú Đô với tổng chiều dài 5,57km. Khu vực phải tổ chức đảm bảo an toàn giao thông thuộc phạm vi các đường: Lê Đức Thọ, Lê Quang Đạo
                 (rào chắn toàn bộ nút giao Châu Văn Liêm đến nút giao Tân Mỹ) có chiều dài 4km/2 chiều.",
                'image' => "hinh1.jpg"
            ],  [
                'title' => "Hà Nội cấm nhiều tuyến phố để thi công đường đua F1 ",
                'content' => "Cụ thể, theo Sở GTVT Hà Nội, khu vực xây dựng đường đua xe F1 nằm trong địa giới hành chính gồm các phường Mỹ Đình 1, Mỹ Đình 2, Mễ Trì, Phú Đô với tổng chiều dài 5,57km. Khu vực phải tổ chức đảm bảo an toàn giao thông thuộc phạm vi các đường: Lê Đức Thọ, Lê Quang Đạo
                 (rào chắn toàn bộ nút giao Châu Văn Liêm đến nút giao Tân Mỹ) có chiều dài 4km/2 chiều.",
                'image' => "hinh1.jpg"
            ],  [
                'title' => "Hà Nội cấm nhiều tuyến phố để thi công đường đua F1 ",
                'content' => "Cụ thể, theo Sở GTVT Hà Nội, khu vực xây dựng đường đua xe F1 nằm trong địa giới hành chính gồm các phường Mỹ Đình 1, Mỹ Đình 2, Mễ Trì, Phú Đô với tổng chiều dài 5,57km. Khu vực phải tổ chức đảm bảo an toàn giao thông thuộc phạm vi các đường: Lê Đức Thọ, Lê Quang Đạo
                 (rào chắn toàn bộ nút giao Châu Văn Liêm đến nút giao Tân Mỹ) có chiều dài 4km/2 chiều.",
                'image' => "hinh1.jpg"
            ],  [
                'title' => "Hà Nội cấm nhiều tuyến phố để thi công đường đua F1 ",
                'content' => "Cụ thể, theo Sở GTVT Hà Nội, khu vực xây dựng đường đua xe F1 nằm trong địa giới hành chính gồm các phường Mỹ Đình 1, Mỹ Đình 2, Mễ Trì, Phú Đô với tổng chiều dài 5,57km. Khu vực phải tổ chức đảm bảo an toàn giao thông thuộc phạm vi các đường: Lê Đức Thọ, Lê Quang Đạo
                 (rào chắn toàn bộ nút giao Châu Văn Liêm đến nút giao Tân Mỹ) có chiều dài 4km/2 chiều.",
                'image' => "hinh1.jpg"
            ]
        ];

        foreach($dataNewsRows as $data) {
            $setup->getConnection()->insert($setup->getTable('sm_article'), $data);
        }
    }
}